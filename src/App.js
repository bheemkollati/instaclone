import { Button, Input, makeStyles, Modal } from '@material-ui/core';
import React, {useState, useEffect} from 'react';
import './App.css';
import { db, auth } from './firebase'
import Post from './Post';
import ImageUpload from './ImageUpload';
import InstagramEmbed from 'react-instagram-embed';

function getModalStyle() {
  const top = 50 ;
  const left = 50;

  return {
    top: `${top}%`,
    left: `${left}%`,
    transform: `translate(-${top}%, -${left}%)`,
  };
}

const useStyles = makeStyles((theme) => ({
  paper: {
    position: 'absolute',
    width: 400,
    backgroundColor: theme.palette.background.paper,
    border: '2px solid #000',
    boxShadow: theme.shadows[5],
    padding: theme.spacing(2, 4, 3),
  },
}));


function App() {

  const [modalStyle] =useState(getModalStyle);

  const classes =useStyles();
  const [posts,setPosts]=useState([]);
  const [open,setOpen]=useState(false);

  const [email,setEmail]=useState('');
  const [password,setPassword] =useState('');
  const [username,setUsername]=useState('');
  const [user,setUser]=useState(null);

  const [openSignIn, setOpenSignIn] =useState(false);

  useEffect(() => {
      const unsubscribe = auth.onAuthStateChanged((authUser) => {
        if(authUser)
        {
          //user has logged in...
            console.log(authUser);
            setUser(authUser);
        }
        else {
            setUser(null);
        }

        return () => {
          //perform some cleanup action before u refresh
          unsubscribe();
        }

      })
  },[user,username]);

  useEffect(() => {
     db.collection('posts').onSnapshot(snapshot  => {
       setPosts(snapshot.docs.map(doc => ({
         id:doc.id,
         post:doc.data()
       })));
     })
  },[posts]);

  const signUp = (event) => {
      event.preventDefault();

      auth
      .createUserWithEmailAndPassword(email,password)
      .then((authUser) => {
        return authUser.user.updateProfile({
          displayName:username,
        })
      })
      .catch((error) => alert(error.message));

      setOpen(false);
  } 

  const signIn = (event) =>{
    event.preventDefault();

    auth
    .signInWithEmailAndPassword(email,password)
    .catch((error) => alert(error.message));

    setOpenSignIn(false);
  }

  return (
    <div className="app">
      
      <Modal
        open={open}
        onClose={() => setOpen(false)}
        >
          <div style={modalStyle} className={classes.paper}>
            <form className="app__signup">
             <center>
              <img className="app__headerImage"
              src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png" 
              alt=""/>
              </center>

              <Input 
                placeholder="username"
                type="text"
                value={username}
                onChange={(e) => setUsername(e.target.value)}
              />

              <Input 
                placeholder="email"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />

              <Input 
                placeholder="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />

              <Button onClick={signUp} type="submit">Login</Button>

            </form>
          
    </div>
      </Modal>
      <Modal
        open={openSignIn}
        onClose={() => setOpenSignIn(false)}
        >
          <div style={modalStyle} className={classes.paper}>
            <form className="app__signup">
             <center>
              <img className="app__headerImage"
              src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png" 
              alt=""/>
              </center>
              <Input 
                placeholder="email"
                type="text"
                value={email}
                onChange={(e) => setEmail(e.target.value)}
              />

              <Input 
                placeholder="password"
                type="password"
                value={password}
                onChange={(e) => setPassword(e.target.value)}
              />

              <Button onClick={signIn} type="submit">Login</Button>

            </form>
          
    </div>
      </Modal>
      <div className="app__header">
          <img className="app__headerImage"
          src="https://www.instagram.com/static/images/web/mobile_nav_type_logo.png/735145cfe0a4.png" 
          alt=""/>

        {
        user ? (
          <Button onClick={() => auth.signOut()}>Log Out</Button>
        ):(
          <div className="app__logincontainer">
            <Button onClick={() => setOpenSignIn(true)}>Sign In</Button>
            <Button onClick={() => setOpen(true)}>Sign Up</Button>
          </div>
        )
      }
      </div>

      <div className="app__posts">
        <div className="app__posstLeft">

              {
                posts.map(({id,post}) => (<Post key={id} postId={id} username={post.username} caption={post.caption} user={user} imageUrl={post.imageUrl} />))
              }
          
        </div>
        <div className="app__postsRight">
              <InstagramEmbed 
              url='https://www.instagram.com/p/CE9vNKtD8Ei/?utm_source=ig_web_copy_link'
              maxWidth={320}
              hideCaption={false}
              containerTagName='div'
              protocol=''
              injectScript
              onLoading={() => {}}
              onSuccess={() =>{}}
              onFailure={() =>{}}
              />

        </div>
      </div>
     
   {user?.displayName ? (
          <ImageUpload  username={user.displayName}/>
        ):
        <h3>sorry you need to login</h3>
        
      }
    </div>
    
  );
}

export default App;
