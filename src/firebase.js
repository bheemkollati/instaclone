import firebase from "firebase";

const firebaseApp=firebase.initializeApp({
    apiKey: "AIzaSyA7aqFCHS7cEX7-HMmLJrb8RvfGMpVo-zA",
    authDomain: "insta-clone-31657.firebaseapp.com",
    projectId: "insta-clone-31657",
    storageBucket: "insta-clone-31657.appspot.com",
    messagingSenderId: "441370053205",
    appId: "1:441370053205:web:24fed07b2c2cd8d2f762dc",
    measurementId: "G-RCF5N5ZDPS"
  });

const db=firebaseApp.firestore();
const auth=firebase.auth();
const storage=firebase.storage();



export {db,auth,storage};